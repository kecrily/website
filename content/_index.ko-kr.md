---
date: "2016-11-08T16:00:00+02:00"
title: "환영합니다"
weight: 10
toc: false
draft: false
url: "ko-kr"
type: "home"
---

<h1 class="title is-1">Gitea - Git with a cup of tea</h1>
<h3 class="subtitle is-3">A painless self-hosted Git service.</h3>
<h4 class="subtitle">
	Gitea는 커뮤니티에서 관리하는 [Gogs](https://gogs.io/)의 [포크](https://blog.gitea.io/2016/12/welcome-to-gitea/)이며,
	[Go](https://golang.org/)로 작성되고 [MIT](https://github.com/go-gitea/gitea/blob/master/LICENSE)로 배포되는 가벼운 코드 호스팅 솔루션입니다.
</h4>

<div class="container">
<a class="button is-success is-large" href="https://try.gitea.io" target="_blank">Gitea 써보기</a>
<a class="button is-light is-large" href="https://docs.gitea.io">문서</a>
</div>
